# Zotero downloader
This is a python module to download pdfs from Zotero directly. The module downloads papers either 
from bioRxiv or from sci-hub. 

To use this module, create your bibliography on Zotero, then export your references
as a BibTeX file. Once the file is generated, run the `bib_downloader.py` file as follows:

```bash
python bib_downloader.py -b BIB_FILE -o OUTPUT_DIR
```

The script will download all pdf iles into `OUTPUT_DIR`. The format of the files is
`X_Y_Z.pdf`, being `X` the first author surname, `Y` the year of publication, and `Z` the first
word on the title (second if the first is a word like "A" or "The"), just to avoid downloading
different papers from the same author on the same year.

If a file exists on `OUTPUT_DIR`, it will not be downloaded, just in case the previously downloaded
file contains annotations or has been modified.