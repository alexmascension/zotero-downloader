import os
import subprocess
import logging
import re


def download_biorxiv(url, out, ID):
    subprocess.run(["wget", url, '-O', f"{out}/{ID}.pdf"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def download_scihub(url, out, ID):
    scihub_output = subprocess.check_output(["wget", url, '-qO', "-"]).decode('utf-8')

    x = re.findall('iframe src = "https://.*.pdf#', scihub_output)

    if len(x) == 0:
        x = re.findall('iframe src = "//.*.pdf#', scihub_output)

    if len(x) > 0:
        link_download = x[0][len('iframe src = "'):-1]
        if link_download[:2] == '//':
            link_download = 'https:' + link_download

        logging.info(f'Found sci-hub URL at {link_download}!')

        subprocess.run(["wget", link_download, '-O', f"{out}/{ID}.pdf"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    else:
        logging.error(f"We could not extract the article from {url}")


