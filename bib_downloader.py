import bibtexparser
from bibtexparser.bparser import BibTexParser
import logging
import os, sys

from funcs import download_biorxiv, download_scihub

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

parser = ArgumentParser()
parser.add_argument('-b', type=str, dest='bib_file',)
parser.add_argument('-o', type=str, dest='out',)

args = parser.parse_args()

bib_file = args.bib_file
out = args.out

os.makedirs(out, exist_ok=True)

logging.info(f"Parsing {bib_file} file, and downloading contents in {out}.")

"""
Parse the bibfile. The result is a list where each element is a dictionary with several elements.
If the type of element is a paper, the element should have a doi and an ID. The ID will be the name of the download file,
and the doi will be used to download the paper via sci-hub.

The ID must be unique, and will be used to check that pdf files already exist in the download directory. If they do, 
they won't be downloaded. If the element does not have ID, the ID will be made of X_Y_Z. X = surname of first author.
Y = year of publication, Z = first word of title. 
"""

parser = BibTexParser(common_strings=False)
parser.ignore_nonstandard_types = False # To include bioRxiv papers, since Zotero flags them as reports.
parser.homogenise_fields = False

with open(bib_file, 'r') as f:
    bib_database = bibtexparser.loads(f.read(), parser)

for element in tqdm(bib_database.entries):
    year = element['date'][:4]
    surname = element['author'].split(',')[0].lower()
    surname = surname.split(' and ')[0].lower().replace(' ', '-')  # In case they are consortia or people without name
    title = element['title'].split(' ')[0:2]
    title_first = title[0].lower() if title[0].lower() not in ['a', 'the'] else title[1].lower()
    ID = f"{surname}_{year}_{title_first}"

    # If there's a / in the title, Windows does not recognize the name of the file!
    ID = ID.replace('/', '-').replace('{', '').replace('}', '')

    logging.info(f"Checking {ID}")

    if os.path.exists(f'{out}/{ID}.pdf'):
        logging.info(f"{out}/{ID}.pdf already exists. Skipping")

    else:
        """
        Recognize content. If the entry is a biorxiv element, it should be downloaded differently than the
        rest of articles, which will be downloaded using sci-hub.
        """

        url = element['url']

        if 'arxiv' in url:  # Download via bioRxiv
            download_url = url.replace('http:', 'https:').replace('/abs/', '/pdf/') + '.pdf'

            logging.info(f"Saving arXiv url {download_url} into {out}/{ID}.pdf")
            download_biorxiv(download_url, out, ID)

        elif 'biorxiv' in url:
            download_url = url.replace('http:', 'https:').replace('lookup/doi', 'content')
            if 'full.pdf' not in download_url:
                download_url += 'v1.full.pdf'

            logging.info(f"Saving bioRxiv url {download_url} into {out}/{ID}.pdf")
            download_biorxiv(download_url, out, ID)

        else:  # Download via sci-hub
            doi = element['doi']
            download_url = f"https://sci-hub.do/{doi}"

            logging.info(f"Saving scihub url {download_url} into {out}/{ID}.pdf")
            download_scihub(download_url, out, ID)



